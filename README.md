# Assignment 6

** Parcelable > Serializable **

# Serializable - master branch
# Parcelable - version1 branch

Serializable არის ჯავას სტანტარტული ინტერფეისი, შეგვიძლია override გავუკეთოთ მეთოდებს. ნელია, 
ქმნის ბევრ temporaty ობიექტს,მაგრამ ადვილი დასაიმპლემენტირებელია

``` class User implements Serializable```

Parcelable არის უფრო ოპტიმიზირებული, არის ადნდროიდის ინტერფეისი

```class MyObjects implements Parcelable```