package com.example.assignment_6.activities

import android.os.Bundle
import android.util.Log.d
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.assignment_6.R
import com.example.assignment_6.databinding.ActivityEditBinding
import com.example.assignment_6.extentions.*
import com.example.assignment_6.models.User
import java.io.Serializable

class EditActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEditBinding
    private lateinit var user: User
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        user = intent.getSerializableExtra(getString(R.string.putExtraUser)) as User
        getHints(user)
        binding.saveBtn.setOnClickListener(saveBtnClick)
    }

    private fun isAllFieldsValid(): Boolean {
        return !(binding.emailAddress.text.isNullOrBlank() ||
                binding.firstName.text.isNullOrBlank() ||
                binding.lastName.text.isNullOrBlank() ||
                binding.sex.text.isNullOrBlank() ||
                binding.birthYear.text.isNullOrBlank())
    }

    private fun makeValidation(): Boolean = isAllFieldsValid() &&
            binding.emailAddress.text.toString().emailValid() &&
            binding.birthYear.text.toString().toInt().yearValid() &&
            binding.sex.text.toString().sexTypeValid()

    private fun getHints(user: User) {
        binding.image.setImageResource(user.image)
        binding.firstName.hint = user.firstName
        binding.lastName.hint = user.lastName
        binding.emailAddress.hint = user.emailAddress
        binding.sex.hint = user.sex
        binding.birthYear.hint = user.birthYear.toString()
    }

    private val saveBtnClick = View.OnClickListener {
        if (makeValidation()) {
            binding.info.gone()
            user.changeInfo(
                binding.firstName.text.toString(),
                binding.lastName.text.toString(),
                binding.emailAddress.text.toString(),
                binding.sex.text.toString(),
                binding.birthYear.text.toString().toInt()
            )
            val intent = intent
            intent.putExtra(getString(R.string.putExtraUser), user as Serializable)
            setResult(RESULT_OK, intent)
            finish()
        } else {
            binding.info.visible()
        }
    }
}

