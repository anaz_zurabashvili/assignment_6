package com.example.assignment_6.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import com.example.assignment_6.R
import com.example.assignment_6.databinding.ActivityMainBinding
import com.example.assignment_6.models.User
import java.io.Serializable

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val userStatic by lazy {
        User(
            R.drawable.avatar,
            getString(R.string.userFirstName),
            getString(R.string.userLastName),
            getString(R.string.userEmailAddress),
            getString(R.string.userSex),
            getString(R.string.userBirthYear).toInt(),
        )
    }
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        user = userStatic
        getUserInfo(user)
        binding.editProfileBtn.setOnClickListener(editProfileBtnClick)
    }

    private fun getUserInfo(user: User) {
        binding.image.setImageResource(user.image)
        binding.firstName.text = user.firstName
        binding.lastName.text = user.lastName
        binding.emailAddress.text = user.emailAddress
        binding.sex.text = user.sex
        binding.birthYear.text = user.birthYear.toString()
    }

    private val editProfileBtnClick = View.OnClickListener {
        val intent = Intent(this, EditActivity::class.java)
        intent.putExtra(getString(R.string.putExtraUser), user as Serializable)
        startForResult.launch(intent)
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                user = (it.data?.extras?.getSerializable(getString(R.string.putExtraUser))
                    ?: user) as User
                getUserInfo(user)
            }
        }
}