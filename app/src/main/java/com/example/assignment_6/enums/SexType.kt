package com.example.assignment_6.enums

enum class SexType {
    FEMALE,
    MALE,
    OTHER
}