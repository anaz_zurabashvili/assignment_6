package com.example.assignment_6.extentions;

import com.example.assignment_6.enums.SexType
import java.util.*


fun String.emailValid(): Boolean = android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun Int.yearValid(): Boolean = this in 1901..Calendar.getInstance().get(Calendar.YEAR)

fun String.sexTypeValid(): Boolean = this.trim().uppercase() in SexType.values().map { it.name }


