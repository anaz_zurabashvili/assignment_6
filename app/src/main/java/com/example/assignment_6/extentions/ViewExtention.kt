package com.example.assignment_6.extentions

import android.view.View

fun View.goneIf(isGone: Boolean) = if (isGone) gone() else visible()

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }
