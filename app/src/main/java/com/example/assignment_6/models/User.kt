package com.example.assignment_6.models

import androidx.annotation.DrawableRes
import java.io.Serializable

class User(
    @DrawableRes
    val image: Int,
    var firstName: String,
    var lastName: String,
    var emailAddress: String,
    var sex: String,
    var birthYear: Int // todo years
) : Serializable {

    fun getInfo(): String =
        "$firstName $lastName \n $emailAddress \n $sex $birthYear"

    fun changeInfo(
        firstNameChanged: String,
        lastNameChanged: String,
        emailAddressChanged: String,
        sexChanged: String,
        birthYearChanged: Int
    ) {
        firstName = firstNameChanged
        lastName = lastNameChanged
        emailAddress = emailAddressChanged
        sex = sexChanged
        birthYear = birthYearChanged
    }

}
